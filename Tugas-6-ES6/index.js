//soal 1
const persegiPanjang = (panjang, lebar) => {
  let luasPersegi = panjang * lebar;

  let kelilingPersegi = 2 * (panjang + lebar);
  return `Panjang persegi adalah: ${luasPersegi} dan keliling persegi adalah: ${kelilingPersegi}`;
};

console.log(persegiPanjang(5, 2));

//soal 2
const newFunction = (firstName, lastName) => {
  awal = firstName;
  kedua = lastName;
  const fullname = firstName + " " + lastName;
  return fullname;
};
console.log(newFunction("Valin", "Fernanda"));

//soal 3
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
};

const { firstName, lastName, address, hobby } = newObject;

console.log(firstName, lastName, address, hobby);

//soal 4
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];

console.log(combined);

//soal 5
const planet = "earth";
const view = "glass";
// var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet
const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit ${planet}`;
console.log(before);
