//soal 1

// pilih angka dari 0 sampai 100, misal 75. lalu isi variabel tersebut dengan angka tersebut. lalu buat lah pengkondisian dengan if-elseif dengan kondisi
// nilai >= 85 indeksnya A
// nilai >= 75 dan nilai < 85 indeksnya B
// nilai >= 65 dan nilai < 75 indeksnya c
// nilai >= 55 dan nilai < 65 indeksnya D
// nilai < 55 indeksnya E

function inputNilai(nilai) {
  if (nilai >= 85) {
    return "A";
  } else if (nilai >= 75 && nilai < 85) {
    return "B";
  } else if (nilai >= 65 && nilai < 75) {
    return "C";
  } else if (nilai >= 55 && nilai < 65) {
    return "D";
  } else if (nilai < 55) {
    return "E";
  }
}

console.log(inputNilai(83));

//soal 2
// ganti tanggal ,bulan, dan tahun sesuai dengan tanggal lahir anda dan buatlah switch case pada bulan, lalu muncul kan string nya dengan output seperti ini 22 Juli 2020
// (isi di sesuaikan dengan tanggal lahir masing-masing)

var tanggal = 8;
var bulan = 1;
var tahun = 1997;

switch (bulan) {
  case 1: {
    bulan = "January";
    break;
  }
  case 2: {
    bulan = "February";
    break;
  }
  case 3: {
    bulan = "Maret";
    break;
  }
  case 4: {
    bulan = "April";
    break;
  }
  case 5: {
    bulan = "Mei";
    break;
  }
  case 6: {
    bulan = "Juni";
    break;
  }
  case 7: {
    bulan = "Juli";
    break;
  }
  case 8: {
    bulan = "Agustus";
    break;
  }
  case 9: {
    bulan = "September";
    break;
  }
  case 10: {
    bulan = "Oktober";
    break;
  }
  case 11: {
    bulan = "November";
    break;
  }
  case 12: {
    bulan = "Desember";
    break;
  }
  default:
    console.log("ngek");
}

console.log(tanggal, bulan, tahun);

//soal 3
function segitiga1(panjang) {
  let hasil = "";
  for (let i = 0; i < panjang; i++) {
    for (let j = 0; j <= i; j++) {
      hasil += "# ";
    }
    hasil += "\n";
  }
  return hasil;
}
console.log(segitiga1(7));

//soal 4

var m = 7;

for (var i = 1; i <= m; i++) {
  var kalimat = "";
  if (i % 3 == 1) {
    kalimat = i + " - I love programming";
  } else if (i % 3 == 2) {
    kalimat = i + " - I love Javascript";
  } else {
    kalimat = i + " - I love VueJS";
    var samaDengan = "\n";

    for (var j = 0; j < i; j++) {
      samaDengan += "=";
    }

    kalimat = kalimat + samaDengan;
  }
  console.log(kalimat);
}
