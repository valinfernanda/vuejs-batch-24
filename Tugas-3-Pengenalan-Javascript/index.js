//soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
// gabungkan variabel-variabel tersebut agar menghasilkan output
// saya senang belajar JAVASCRIPT

var first = pertama.substring(0, 4);
var second = pertama.substring(12, 18);
var third = kedua.substring(0, 7);
var forth = kedua.substring(8, 18);
var upperCase = forth.toUpperCase(forth);

console.log(first, second, third, upperCase);

// soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";
var numSatu = Number(kataPertama);
var numDua = Number(kataKedua);
var numTiga = Number(kataKetiga);
var numEmpat = Number(kataKeempat);
var duaPuluhEmpat = numSatu + numDua * numTiga + numEmpat;

console.log(duaPuluhEmpat);

// soal 3

var kalimat = "wah javascript itu keren sekali";

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25, 31);

console.log("Kata Pertama: " + kataPertama);
console.log("Kata Kedua: " + kataKedua);
console.log("Kata Ketiga: " + kataKetiga);
console.log("Kata Keempat: " + kataKeempat);
console.log("Kata Kelima: " + kataKelima);
