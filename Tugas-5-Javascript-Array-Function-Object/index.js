//soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
var hasilSort = daftarHewan.sort();
var ubahKeString = hasilSort.join(" ");
for (var i = 1; i < 2; i++) {
  //   ubahKeString = "\n";

  var satu = console.log(ubahKeString);
}

//soal 2
function introduce(perkenalan) {
  var kelola = perkenalan.name;

  console.log(
    "Nama saya " +
      perkenalan.name +
      ", umur saya " +
      perkenalan.age +
      " tahun, alamat saya di " +
      perkenalan.address +
      ", dan saya punya hobby yaitu " +
      perkenalan.hobby


  );
}

var data = {
  name: "John",
  age: 30,
  address: "Jalan Pelesiran",
  hobby: "Gaming",
};

var perkenalan = introduce(data);
console.log(perkenalan); // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming"

//soal 3
const vowels = ["a", "e", "i", "o", "u"];
function countVowel(str) {
  // initialize count
  let count = 0;
  // loop through string to test if each character is a vowel
  for (let letter of str.toLowerCase()) {
    if (vowels.includes(letter)) {
      count++;
    }
  }
  // return number of vowels
  return count;
}
console.log(countVowel("Valin"));
console.log(countVowel("Laura"));

//soal 4
function hitung(x) {
  var hasil = x * 2 - 2;
  return hasil;
}

console.log(hitung(0));
console.log(hitung(1));
console.log(hitung(2));
console.log(hitung(3));
console.log(hitung(5));
