jadi, untuk penulisan CSS ada 3 cara :

1. Inline CSS - Penulisan CSS langsung di atribut HTML tersebut.
2. External CSS - Menggunakan tag <link href="dirfolder/namefile.css">.
3. Internal CSS - Penulisannya ditulis dalam tag <style> .

h2 {
color: rgb(10, 8, 8);
}

selector : h2
properti : color
value : rgb(10, 8, 8)
